﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel.Activation;
using System.Net;

namespace Enu
{

    [ServiceContract]
    public interface IDataService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
                    RequestFormat = WebMessageFormat.Xml,
                    ResponseFormat = WebMessageFormat.Xml,
                    BodyStyle = WebMessageBodyStyle.Bare,
                    UriTemplate = "/AuthorizeUser")]
        EnuResponse AuthorizeUser(EnuRequest req);

        [OperationContract]
        [WebInvoke(Method = "POST",
                    RequestFormat = WebMessageFormat.Xml,
                    ResponseFormat = WebMessageFormat.Xml,
                    BodyStyle = WebMessageBodyStyle.Bare,
                    UriTemplate = "/TestMethod")]
        [FaultContract(typeof(InvalidInputFault))]
        AuthorizeUser TestMethod(UserCredential data);

        [WebInvoke(Method = "POST",
                    RequestFormat = WebMessageFormat.Xml,
                    ResponseFormat = WebMessageFormat.Xml,
                    BodyStyle = WebMessageBodyStyle.Bare,
                    UriTemplate = "/TestMe1")]
        EnuResponse TestMe1(EnuRequest req);
      
    }

}
