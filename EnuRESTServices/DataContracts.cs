﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
using System.ServiceModel.Activation;

namespace Enu
{
    /// <summary>
    /// Encapsulates HTTP request to POST method.
    /// </summary>
   [DataContract(Namespace = "")]
    public class EnuRequest
    {
        [DataMember]
        public string Tocken { get; set; }

        [DataMember]
        public string UserData { get; set; }
    }

    /// <summary>
    /// Encapsulates HTTP response for POST method.
    /// </summary>
    [DataContract]
    public class EnuResponse
    {
        [DataMember]
        public string Tocken { get; set; }

        [DataMember]
        public string Result { get; set; }

        [DataMember]
        public string UserData { get; set; }
    }


    /// <summary>
    /// Encapsulates error detail to expose.
    /// </summary>
    [DataContract]
    public class Error
    {
        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Desc { get; set; }
    }



    [DataContract]
    public class InvalidInputFault
    {
        [DataMember]
        public int Code { get; set; }

        [DataMember]
        public bool Result { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Description { get; set; }
    }


    [DataContract(Namespace="")]
    public class UserCredential
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string UserPassword { get; set; }
    }


    [DataContract]
    public class AuthorizeUser
    {
        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public string UserType { get; set; }
    }
}