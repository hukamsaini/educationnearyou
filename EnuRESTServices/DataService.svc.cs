﻿using System;
using System.Net;
using System.Dynamic;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ENU.BusinessManager;
using ENU.Entity;

namespace Enu
{
    public class DataService : IDataService
    {
        /// <summary>
        /// Authorize user to by loging to system:
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public EnuResponse AuthorizeUser(EnuRequest req)
        {
            EnuResponse res = new EnuResponse();
            res.Tocken = req.Tocken;
            res.Result = "Fail";
            res.UserData = "";

            try
            {
                IDataAccessor accessor = new DataAccessor();
                UserTicket t = accessor.Login("", "", "");
                res.Result = "OK";
                res.UserData = "This is a test data..";
                //throw new ArgumentException("Failed");
            }
            catch
            {
                res.Result = "Fail";
                res.UserData = string.Format("<Error><Code>{0}</Code><Desc>{1}</Desc></Error>", "101", "This is a test error");
            }

            return res;
        }


        [FaultContract(typeof(InvalidInputFault))]
        public AuthorizeUser TestMethod(UserCredential data)
        {
            try
            {
                var d = data;
                var r = new AuthorizeUser { Token = data.UserPassword + "_PWD", UserType = data.UserName + "_UN" };
                return r;
            }
            catch (DivideByZeroException ex)
            {
                InvalidInputFault fault = new InvalidInputFault();
                fault.Code = 100;
                fault.Result = false;
                fault.Message = ex.Message;
                fault.Description = "Cannot divide by zero (0).";
                throw new FaultException<InvalidInputFault>(fault);
            }
        }


        public EnuResponse TestMe1(EnuRequest req)
        {
            //var res = new EnuResponse { Tocken = "hgjhguytuy09-09-0", Result = "OK", Data = "13890" };
            var res = new EnuResponse { Tocken = req.Tocken, UserData = req.UserData, Result = "OK" };
            //res.Tocken = "hgjhguytuy09-09-0";
            //res.Result = "OK";
            //res.Data = "13890";
            return res;
        }
    }
}
