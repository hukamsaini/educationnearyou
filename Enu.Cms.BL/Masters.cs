﻿using System;
using System.Linq;
using System.Text;
using ENU.Entity;
using Arcmatics.DAL;
using System.Reflection;
using System.Collections.Generic;
using System.Data;

namespace Enu.Cms.BL
{
    public interface ICommonMaster : IDisposable
    {
        bool Save<T>(T obj);

        Board GetBoard();

        List<Board> GetAllBoard(char type, int? value, string action);
    }


    public class CommonMaster : ICommonMaster
    {
        public CommonMaster()
        {
        }

        public void Dispose()
        {

        }

        private bool ManageMaster(int? id, string name, char type, bool active, bool deleted, string metaType, string session)
        {
            // Check for the user tocken:
            if (System.Web.HttpContext.Current.Session["ticket"] == null ||
                string.IsNullOrEmpty(((UserTicket)(System.Web.HttpContext.Current.Session["ticket"])).Tocken))
                throw new ArgumentException("UNKOWN_USER");

            DataProvider objProvider = new DataProvider();
            string status = objProvider.ExecuteScalar(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                            new object[] {id, name, type, active, deleted, metaType,
                                          ((UserTicket)(System.Web.HttpContext.Current.Session["ticket"])).Tocken }, ""), UserType.ADMIN);
            
            return Convert.ToBoolean(Int16.Parse(status));
        }


        public bool Save<T>(T obj)
        {
            if (obj.GetType() == typeof(Board))
            {
                Board board = (Board)Convert.ChangeType(obj, typeof(Board));
                board.Id = String.IsNullOrEmpty(board.Value) ? null : (int?)(int.Parse(board.Value));
                board.MetaType = obj.GetType().Name.ToUpper();
                return this.ManageMaster(board.Id, board.Name, board.Type, board.IsActive, board.Deleted, board.MetaType, string.Empty);
            }

            else
                return false;
        }

        /// <summary>
        /// Creates object of Board type. 
        /// </summary>
        /// <returns></returns>
        public Board GetBoard()
        {
            return new Board();
        }


        /// <summary>
        /// Returns list of bords and detail of a selected board.
        /// </summary>
        public List<Board> GetAllBoard(char type, int? value, string action)
        {
            DataProvider da = new DataProvider();
            DataTable table = da.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(), new object[] { type, value, action }, ""), UserType.ADMIN);

            List<Board> lstBoards;

            if (string.IsNullOrEmpty(action))
            {
                lstBoards = new List<Board>(from b in table.AsEnumerable()
                                            select new Board { Name = b["Name"].ToString(), Value = b["Value"].ToString() });
            }
            else
                lstBoards = new List<Board>(from b in table.AsEnumerable()
                                            select new Board { Name = b["Name"].ToString(), Value = b["Value"].ToString(), Active = b["Active"].ToString() });

            return lstBoards;
        }


        /// <summary>
        /// Returns object of ManageTutorial type. 
        /// </summary>
        /// <returns></returns>
        public ManageTutorial ManageTutorial()
        {
            return new ManageTutorial();
        }
    }
}
