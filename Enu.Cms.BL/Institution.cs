﻿using System;
using System.Web;
using ENU.Entity;
using System.Linq;
using System.Text;
using Arcmatics.DAL;
using System.Reflection;
using System.Collections.Generic;
using System.Data;

namespace Enu.Cms.BL
{
    public interface IInstitution : IDisposable
    {
        InstitutionType GetInstituteTypes();

        void Save<T>(T obj);

        School School();

        College College();

        Tution_Centre TutionCentre();

        DataTable GetAllInstitutes<T>(T obj);
       
    }


    /// <summary>
    /// Class to define set rules for all instituions & related.
    /// </summary>
    public class Institution : IInstitution
    {
        public Institution()
        {
        }

        public void Dispose()
        {

        }


        public InstitutionType GetInstituteTypes()
        {
            InstitutionType itype = new InstitutionType();
            return itype;
        }

        public School School()
        {
            return new School();
        }

        public College College()
        {
            return new College();
        }

        public Tution_Centre TutionCentre()
        {
            return new Tution_Centre();
        }

        /// <summary>
        /// Save all types of institutional body.
        /// </summary>
        /// <typeparam name="T">Institute type</typeparam>
        /// <param name="obj">Object data</param>
        public void Save<T>(T obj)
        {
            if (obj.GetType() == typeof(School))
            {
                School sc = (School)Convert.ChangeType(obj, typeof(School));
                sc.InstituteType = obj.GetType().Name;
                this.SaveInstitute(sc.InstituteId, sc.InstituteType, sc.Name, sc.Address, sc.City, sc.State, sc.Description, sc.Phone1, sc.Phone2,
                                   sc.Email, sc.WebUrl, sc.ImgUrl, sc.EstablishedOn, sc.AffiliationId, sc.Keywords, string.Empty);
            }
            else if (obj.GetType() == typeof(College))
            {
                College sc = (College)Convert.ChangeType(obj, typeof(College));
                sc.InstituteType = obj.GetType().Name;
                this.SaveInstitute(sc.InstituteId, sc.InstituteType, sc.Name, sc.Address, sc.City, sc.State, sc.Description, sc.Phone1, sc.Phone2,
                                   sc.Email, sc.WebUrl, sc.ImgUrl, sc.EstablishedOn, sc.AffiliationId, sc.Keywords, string.Empty);
            }
            else if (obj.GetType() == typeof(Tution_Centre))
            {
                Tution_Centre sc = (Tution_Centre)Convert.ChangeType(obj, typeof(Tution_Centre));
                sc.InstituteType = obj.GetType().Name.Replace("_", " ");
                this.SaveInstitute(sc.InstituteId, sc.InstituteType, sc.Name, sc.Address, sc.City, sc.State, sc.Description, sc.Phone1, sc.Phone2,
                                   sc.Email, sc.WebUrl, sc.ImgUrl, sc.EstablishedOn, sc.AffiliationId, sc.Keywords, string.Empty);
            }
        }


        /// <summary>
        /// Get All Institute According to filter.
        /// </summary>
        /// <typeparam name="T">Institute type</typeparam>
        /// <param name="obj">Object data</param>
        public DataTable GetAllInstitutes<T>(T obj)
        {

            Institute sc = (Institute)Convert.ChangeType(obj, typeof(Institute));
            return GetAllInstitutes(sc.InstituteType);


        }

        // private IList<Institute> GetAllInstitutes(string InstituteType) {
        private DataTable GetAllInstitutes(string InstituteType)
        {
            DataProvider objProvider = new DataProvider();
            DataTable _instituteTable = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                              new object[] { InstituteType }, ""), UserType.ADMIN);

            return _instituteTable;
        }
        /// <summary>
        /// Get All Institute According to filter.
        /// </summary>
        /// <typeparam name="T">Institute type</typeparam>
        /// <param name="obj">Object data</param>
        private void SaveInstitute(int? InstituteId, string InstituteType, string Name, string Address, int City, int State, string Description, string Phone1, string Phone2,
                                    string Email, string WebUrl, string ImgUrl, int EstablishedOn, int Affiliation, string Keywords, string Session)
        {
            // Check for the user tocken:
            if (System.Web.HttpContext.Current.Session["ticket"] == null ||
                string.IsNullOrEmpty(((UserTicket)(System.Web.HttpContext.Current.Session["ticket"])).Tocken))
                throw new ArgumentException("UNKOWN_USER");

            DataProvider objProvider = new DataProvider();
            objProvider.ExecuteQuery(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                            new object[] { InstituteId, InstituteType, Name, Address, City, State, Description, Phone1, Phone2,
                                    Email, WebUrl, ImgUrl, EstablishedOn, Affiliation, Keywords,  
                                    ((UserTicket)(System.Web.HttpContext.Current.Session["ticket"])).Tocken }, ""), UserType.ADMIN);
        }
       
    }
}
