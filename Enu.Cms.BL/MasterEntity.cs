﻿using System;
using ENU.Entity;
using System.Linq;
using System.Text;
using System.Collections.Generic;


namespace Enu.Cms.BL
{
    /// <summary>
    /// Manages the transaction action.
    /// </summary>
    public enum Action
    {
        GET,
        VIEW,
        EDIT,
        DELETE
    }

    /// <summary>
    /// Defines the set of user information who manages data.
    /// </summary>
    public abstract class User
    {
        public Nullable<int> Id { get; set; }
        public string AddedBy { get; set; }
        public string AddedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public bool Deleted { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedOn { get; set; }
        public bool IsActive { get; set; }
        public string Active { get; set; }
        public string MetaType { get; set; }
    }

    /// <summary>
    /// Defines the board/universuty for school/college.
    /// </summary>
    public class Board : User
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public Char Type { get; set; }
    }


    /// <summary>
    /// Defines tutorial and user details who is managing information.
    /// </summary>
    public class ManageTutorial : User
    {
        public Tutorial TutorialDetail { get; set; }
    }
}
