﻿using System;
using System.Web;
using ENU.Entity;
using System.Data;
using System.Linq;
using Arcmatics.DAL;
using System.Reflection;
using System.Collections.Generic;

namespace Enu.Cms.BL
{
    public interface IUserAuthentication : IDisposable
    {
        UserTicket LoginToCMS(string userName, string password, string session);

        void Logout(string session);
    }

    /// <summary>
    /// Class to define set rules for user credentials.
    /// </summary>
    public class UserAuthentication : IUserAuthentication
    {
        public UserAuthentication()
        {
        }

        public void Dispose()
        {
        }


        /// <summary>
        /// Login to system and a tocken.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        public UserTicket LoginToCMS(string userName, string password, string session)
        {
            DataProvider objProvider = new DataProvider();
            DataTable user = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                            new object[] { userName, password, HttpContext.Current.Session.SessionID }, ""), UserType.ADMIN);

            if (user == null)
                throw new ArgumentException("USER_NOT_EXISTS");

            UserTicket ticket = new List<UserTicket>(from u in user.AsEnumerable()
                                                     select new UserTicket { Name = Convert.ToString(u["Name"]), Tocken = u["Tocken"].ToString() }).FirstOrDefault();
            return ticket;
        }


        /// <summary>
        /// Logout from system and clear the tockens.
        /// </summary>
        /// <param name="session"></param>
        public void Logout(string session)
        {
            // Check for the user tocken:
            if (!string.IsNullOrEmpty(((UserTicket)(System.Web.HttpContext.Current.Session["ticket"])).Tocken))
            {
                DataProvider objProvider = new DataProvider();
                objProvider.ExecuteQuery(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                                new object[] { ((UserTicket)(System.Web.HttpContext.Current.Session["ticket"])).Tocken }, ""), UserType.ADMIN);
            }
        }
    }
}
