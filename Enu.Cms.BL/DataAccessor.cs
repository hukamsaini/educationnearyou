﻿using System;
using System.Web;
using System.Linq;
using ENU.Entity;
using System.Text;
using System.Data;
using Arcmatics.DAL;
using System.Reflection;
using System.Collections.Generic;


namespace Enu.Cms.BL
{
    /// <summary>
    /// Base class to instantiate required object type.
    /// </summary>
    public class DataAccessor : IDataAccessor
    {
        public DataAccessor()
        {
        }

        public void Dispose()
        {
        }
        
        public IUserAuthentication GetUserAuthentication()
        {
            return new UserAuthentication();
        }

        public IInstitution GetInstitution()
        {
            return new Institution();
        }

        public ILodging GetLodging()
        {
            return new Lodging();
        }

        public IMasterDataHelper GetMasterDataHelper()
        {
            return new MasterDataHelper();
        }

        public ICommonMaster GetCommonMaster()
        {
            return new CommonMaster();
        }
    }
}
