﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ENU.Entity.Common;

namespace Enu.Cms.BL
{
    public interface IMasterDataHelper
    {
        List<City> GetAllCity();
        List<City> GetAllCity(int stateId);
        List<State> GetAllState();
        List<State> GetAllState(int countryId);
        List<Country> GetAllCountry();
        List<EnuCategories> GetEnuCategories();
        List<EnuMasterType> GetEnuMasterTypes();
        List<Subject> GetAllSubject();
        List<Facility> GetAllFacility();
        List<CourseCategory> GetAllCourseCategory();
        //List<Board> GetEnuBoardUniversity();
        void SaveMasterData(int typeId, string typeValue, int stateId, int countryId);
    }
}
