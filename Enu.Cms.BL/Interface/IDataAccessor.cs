﻿using System;
using ENU.Entity;
using System.Linq;
using System.Text;
using System.Collections.Generic;

// The base interface:
namespace Enu.Cms.BL
{
    public interface IDataAccessor : IDisposable
    {
        IUserAuthentication GetUserAuthentication();

        IInstitution GetInstitution();

        ILodging GetLodging();

        IMasterDataHelper GetMasterDataHelper();

        ICommonMaster GetCommonMaster();
    }
}
