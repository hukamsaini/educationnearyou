﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ENU.Entity.Common;
using Arcmatics.DAL;
using System.Reflection;
using System.Data;

namespace Enu.Cms.BL
{

    public class MasterDataHelper : IMasterDataHelper
    {
        public List<Country> GetAllCountry()
        {
            DataProvider objProvider = new DataProvider();
            DataTable _enuCoutries = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                              null, "Select CountryId, CountryName, IsActive from tbl_LookUpCountry"), UserType.ADMIN);
            return objProvider.ConvertDataTable<Country>(_enuCoutries);
        }

        public List<State> GetAllState(int countryId)
        {
            DataProvider objProvider = new DataProvider();
            DataTable _enuStates = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                              null, "Select StateId, StateName, CountryId from tbl_LookUpState where CountryId = " + countryId + ""), UserType.ADMIN);
            return objProvider.ConvertDataTable<State>(_enuStates);
        }

        public List<State> GetAllState()
        {
            DataProvider objProvider = new DataProvider();
            DataTable _enuStates = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                               null, "Select StateId, StateName, CountryId from tbl_LookUpState"), UserType.ADMIN);
            return objProvider.ConvertDataTable<State>(_enuStates);
        }

        public List<City> GetAllCity(int stateId)
        {
            DataProvider objProvider = new DataProvider();
            DataTable _cititesList = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                              null, "Select CityId, CityName, StateId from tbl_LookUpcity where StateId = " + stateId + ""), UserType.ADMIN);
            return objProvider.ConvertDataTable<City>(_cititesList);
        }

        public List<City> GetAllCity()
        {
            DataProvider objProvider = new DataProvider();
            DataTable _enuCitites = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                              null, "Select * from vw_GetAllCity"), UserType.ADMIN);
            return objProvider.ConvertDataTable<City>(_enuCitites);
        }

        public List<EnuCategories> GetEnuCategories()
        {
            DataProvider objProvider = new DataProvider();
            DataTable _eunCategories = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                              null, "Select CategoryId,CategoryName from tbl_LookUpCategory"), UserType.ADMIN);

            var _eunCategoriesList = objProvider.ConvertDataTable<EnuCategories>(_eunCategories);
            return _eunCategoriesList;
        }

        
        public List<EnuMasterType> GetEnuMasterTypes()
        {
            return MasterTypeHelper.GetMasterTypes();
        }

        public void SaveMasterData(int typeId, string typeValue, int stateId, int countryId)
        {

            string sqlCommandText = GetInsertSQLCommand(typeId, typeValue, stateId, countryId);
            if (!string.IsNullOrEmpty(sqlCommandText))
            {
                var sqlCommand = AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                                  null, sqlCommandText);
                DataProvider objProvider = new DataProvider();
                objProvider.ExecuteQuery(sqlCommand, UserType.ADMIN);
            }
        }

        private string GetInsertSQLCommand(int typeId, string typeValue, int stateId = 0, int countryId = 0)
        {
            string sqlCommand = string.Empty;
            MasterTypes masterType = (MasterTypes)typeId;

            // Check MasterDataHelper for typeId and their value
            switch (masterType)
            {
                case MasterTypes.Subject: // School
                    sqlCommand = string.Format(@"If Not Exists (Select * from tbl_lookUpSubject where Name='{0}')
                                    BEGIN
	                                    Insert into tbl_lookUpSubject (Name) Values ('{0}')
                                    END", typeValue);
                    break;

                case MasterTypes.CourseCategory: // Course
                    sqlCommand = string.Format(@"If Not Exists (Select * from tbl_LookUpCourseCategory where Name='{0}')
                                    BEGIN
	                                    Insert into tbl_LookUpCourseCategory (Name) Values ('{0}')
                                    END", typeValue);
                    break;

                case MasterTypes.Facility: // Facility
                    sqlCommand = string.Format(@"If Not Exists (Select * from tbl_LookUpFacilityType where Name='{0}')
                                    BEGIN
	                                    Insert into tbl_LookUpFacilityType (Name) Values ('{0}')
                                    END", typeValue);
                    break;

                case MasterTypes.City: // City
                    sqlCommand = string.Format(@"If Not Exists (Select * from tbl_LookUpCity where CityName='{0}')
                                    BEGIN
	                                    Insert into tbl_LookUpCity (CityName,StateId) Values ('{0}',{1})
                                    END", typeValue, stateId);
                    break;

                case MasterTypes.State: // State
                    sqlCommand = string.Format(@"If Not Exists (Select * from tbl_LookUpState where StateName='{0}')
                                    BEGIN
	                                    Insert into tbl_LookUpState (StateName,CountryId) Values ('{0}',{1})
                                    END", typeValue, countryId);
                    break;

                case MasterTypes.Country: //  Country                   
                    sqlCommand = string.Format(@"If Not Exists (Select * from tbl_LookupCountry where CountryName='{0}')
                                    BEGIN
	                                    Insert into tbl_LookupCountry (CountryName,IsActive) Values ('{0}',1)
                                    END", typeValue);
                    break;

                default:
                    break;

            }

            return sqlCommand;
        }

        public List<Board> GetEnuBoardUniversity()
        {
            return new List<Board>();
        }

        public List<Subject> GetAllSubject()
        {
            DataProvider objProvider = new DataProvider();
            DataTable _enuSubjects = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                              null, "Select SubjectId, Name from tbl_LookUpSubject order by SubjectId"), UserType.ADMIN);

            return objProvider.ConvertDataTable<Subject>(_enuSubjects);
        }

        public List<Facility> GetAllFacility()
        {
            DataProvider objProvider = new DataProvider();
            DataTable _enuFacilities = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                              null, "Select FacilityTypeId, Name from tbl_LookUpFacilityType order by FacilityTypeId"), UserType.ADMIN);

            return objProvider.ConvertDataTable<Facility>(_enuFacilities);
        }

        public List<CourseCategory> GetAllCourseCategory()
        {
            DataProvider objProvider = new DataProvider();
            DataTable _enuFacilities = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                              null, "Select CourseCategoryId, Name from tbl_LookUpCourseCategory order by CourseCategoryId"), UserType.ADMIN);

            return objProvider.ConvertDataTable<CourseCategory>(_enuFacilities);
        }
    }
}
