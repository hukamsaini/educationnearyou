﻿using System;
using ENU.Entity;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Enu.Cms.BL
{
    public interface ILodging : IDisposable
    {
        void GetLodge();
    }


    /// <summary>
    /// Class to define set rules for all lodging facilities.
    /// </summary>
    public class Lodging : ILodging
    {
        public Lodging()
        {
        }

        public void Dispose()
        {
        }

        public void GetLodge()
        {

        }
    }
}
