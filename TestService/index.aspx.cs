﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TestService.ServiceReference1;
using System.ServiceModel;

namespace TestService
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var ts = new ServiceReference1.DataServiceClient();
                EnuResponse r = ts.AuthorizeUser(new EnuRequest { Tocken = "12", UserData = "Test" });
            }
            catch (FaultException<InvalidInputFault> ex)
            {
                Response.Write(ex.Detail.Description);
            }
        }
    }
}