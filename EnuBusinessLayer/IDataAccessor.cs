﻿using System;
using ENU.Entity;
using System.Linq;
using System.Text;
using System.Collections.Generic;


namespace ENU.BusinessManager
{
    public interface IDataAccessor : IDisposable
    {
        UserTicket Login(string email, string pwd, string tocken);
    }
}
