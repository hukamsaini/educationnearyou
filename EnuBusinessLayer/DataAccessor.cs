﻿using System;
using System.Web;
using System.Linq;
using ENU.Entity;
using System.Text;
using System.Data;
using Arcmatics.DAL;
using System.Reflection;
using System.Collections.Generic;


namespace ENU.BusinessManager
{
    public class DataAccessor : IDataAccessor
    {
        public DataAccessor()
        {
        }

        public void Dispose()
        {
        }


        public UserTicket Login(string userName, string password, string session)
        {
            DataProvider objProvider = new DataProvider();
            DataTable user = objProvider.GetDataTable(AseCommandGenerator.GenerateCommand((MethodInfo)MethodBase.GetCurrentMethod(),
                            new object[] { userName, password, HttpContext.Current.Session.SessionID }, ""), UserType.ADMIN);


            UserTicket ticket = new List<UserTicket>(from u in user.AsEnumerable()
                                                     select new UserTicket { Email = Convert.ToString(u["Name"]), Tocken = u["Tocken"].ToString() }).FirstOrDefault();
            return ticket;
        }
    }
}
