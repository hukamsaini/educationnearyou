using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Collections.Generic;

// Author: Navin C. Pandit
// This class used to read sql connections.
//
namespace Arcmatics.DAL
{
    public sealed class DataProvider
    {
        // Set configuration file path to read:
        private const string CONF_FILE = "Web.config";


        /// Protect connection class from out-side world.
        /// 
        /// <summary>
        /// Initialise database connection for public users.
        /// </summary>
        /// <returns>String</returns>
        private SqlConnection GetDbConnection(UserType userType)
        {
            String _connection = string.Empty;
            string path = string.Empty;
            _connection = ConfigurationManager.ConnectionStrings[userType.ToString()].ToString();
            SqlConnection connection = new SqlConnection(_connection);
            return connection;
        }



        /// <summary>
        /// This method returns dataset object filled with tabular data w.r.t. command
        /// </summary>
        /// <param name="command">Sql command having procedures and parameters</param>
        /// <returns></returns>
        public DataSet GetDataSet(SqlCommand command, UserType userType)
        {
            command.Connection = GetDbConnection(userType);
            DataSet _ds = new DataSet();
            SqlDataAdapter _da = new SqlDataAdapter(command);
            _da.Fill(_ds);
            return _ds;
        }


        /// <summary>
        /// This method returns data table object filled with record set w.r.t. command
        /// </summary>
        /// <param name="command">Sql command having procedures and parameters</param>
        /// <returns></returns>
        public DataTable GetDataTable(SqlCommand command, UserType userType)
        {
            command.Connection = GetDbConnection(userType);
            DataSet _ds = new DataSet();
            SqlDataAdapter _da = new SqlDataAdapter(command);
            _da.Fill(_ds);

            if (_ds != null && _ds.Tables.Count > 0)
                return _ds.Tables[0];
            else
                return null;
        }


        /// <summary>
        /// This method returns single value after execution of command
        /// </summary>
        /// <param name="command">Sql command having procedures and parameters</param>
        /// <returns></returns>
        public string ExecuteScalar(SqlCommand command, UserType userType)
        {
            command.Connection = GetDbConnection(userType);

            string value = string.Empty;

            try
            {
                command.Connection.Open();
                value = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                command.Connection.Close();
                command.Connection.Dispose();
                throw new ArgumentException("Data provider error", ex);
            }

            return value;
        }


        /// <summary>
        /// This method returns no value after execution of command
        /// </summary>
        /// <param name="command">Sql command having procedures and parameters</param>
        /// <returns></returns>
        public void ExecuteQuery(SqlCommand command, UserType userType)
        {
            command.Connection = GetDbConnection(userType);

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                command.Connection.Close();
                command.Connection.Dispose();
                throw new ArgumentException("Data provider error", ex);
            }
        }


        /// <summary>
        /// This method returns single value after execution of command.
        /// It needs open connection to managa transaction rollback.
        /// </summary>
        /// <param name="command">Sql command having procedures and parameters</param>
        /// /// <param name="returnValue">Default value false.</param>
        /// <returns></returns>
        public string ExecuteTransactions(SqlCommand command, Boolean returnValue, UserType userType)
        {
            command.Connection = GetDbConnection(userType);

            if (returnValue)
            {
                string value = string.Empty;
                value = Convert.ToString(command.ExecuteScalar());
                return value;
            }
            else
            {
                command.ExecuteNonQuery();
                return "-1";
            }
        }


        /// <summary>
        /// This method will convert datatable to a list.
        /// </summary>
        /// <param name="command">Datatble</param>
        /// <returns>List of object</returns>
        public List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        } 
    }


    /// <summary>
    /// Set connection string key to read:
    /// </summary>
    public enum UserType
    {
        // Open users:
        USER = 0,

        // Registered users:
        MEMBER,

        // Admin:
        ADMIN
    }
}
