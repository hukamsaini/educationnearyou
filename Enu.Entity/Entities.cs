﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ENU.Entity
{

    /// <summary>
    /// Login to system.
    /// </summary>
    public class Login
    {
        [Required(ErrorMessage = "Please enter e-mail", AllowEmptyStrings = false)]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Invalid e-mail")]
        public String Email { get; set; }

        [Required(ErrorMessage = "Please enter password", AllowEmptyStrings = false)]
        public String Password { get; set; }
    }


    /// <summary>
    /// User ticket after successfull credential.
    /// </summary>
    public class UserTicket
    {
        public String Email { get; set; }
        public String Tocken { get; set; }
        public String Name { get; set; }
        public String UType { get; set; }
        public String Id { get; set; }
    }


    /// <summary>
    /// Defines tutorial/study material
    /// </summary>
    public class Tutorial
    {
        public string Name { get; set; }
        public string Desc { get; set; }
        public string Course { get; set; }
        public string Year { get; set; }
        public string Board { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }
        public string Value { get; set; }
        public bool IsActive { get; set; }
    }


    /// <summary>
    /// Board or university type.
    /// </summary>
    public class University
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}

