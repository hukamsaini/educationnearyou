﻿using System;

namespace ENU.Entity
{
    /// <summary>
    /// Defines the types of instutions w.r.t its value.
    /// </summary>
    public class InstitutionType
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    
    /// <summary>
    /// Defines object type for Institution
    /// </summary>
    public class Institute
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int AffiliationId { get; set; }
        public string AffiliationName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string WebUrl { get; set; }
        public string ImgUrl { get; set; }
        public string Address { get; set; }
        public int State { get; set; }
        public int City { get; set; }
        public string PinCode { get; set; }
        public int Country { get; set; }
        public int EstablishedOn { get; set; }
        public string InstituteType { get; set; }
        public Nullable<int> InstituteId { get; set; }
        public string Keywords { get; set; }
    }

    public class School : Institute
    {
        public string ClassesBetween { get; set; }
    }

    public class College : Institute
    {
        public string Streams { get; set; }
    }

    public class Tution_Centre : Institute
    {
        string TargetedCourse { get; set; }
        string Classes { get; set; }
    }

}
