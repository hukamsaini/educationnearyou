﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENU.Entity.Common
{   
    public class CourseCategory : BaseEnuEntity
    {
        public int CourseCategoryId { get; set; }
        public string Name { get; set; }
    }
}
