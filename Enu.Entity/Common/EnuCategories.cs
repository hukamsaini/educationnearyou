﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENU.Entity.Common
{
    public class EnuCategories
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
