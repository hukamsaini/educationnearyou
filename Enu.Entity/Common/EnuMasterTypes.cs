﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENU.Entity.Common
{
    public class EnuMasterType
    {
        public int TypeId { get; set; }
        public string TypeName { get; set; }
    }

    public enum MasterTypes
    {
        Subject = 1,
        CourseCategory = 2,
        Facility = 3,
        City = 4,
        State = 5,
        Country = 6
    }

    public static class MasterTypeHelper
    {
        public static List<EnuMasterType> GetMasterTypes()
        {
            var _list = new List<EnuMasterType>();

            _list.Add(new EnuMasterType { TypeId = 0, TypeName = "Select One" });
            _list.Add(new EnuMasterType { TypeId = 1, TypeName = "Subject" });
            _list.Add(new EnuMasterType { TypeId = 2, TypeName = "CourseCategory" });
            _list.Add(new EnuMasterType { TypeId = 3, TypeName = "Facility" });
            _list.Add(new EnuMasterType { TypeId = 4, TypeName = "City" });
            _list.Add(new EnuMasterType { TypeId = 5, TypeName = "State" });
            _list.Add(new EnuMasterType { TypeId = 6, TypeName = "Country" });

            return _list;
        }
    }
}
