﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENU.Entity.Common
{
    public class Country : BaseEnuEntity
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }
}
