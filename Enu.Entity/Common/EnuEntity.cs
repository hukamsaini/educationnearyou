﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENU.Entity
{
    public interface IEnuEntity
    {       
        bool IsActive { get; set; }
    }

    public abstract class BaseEnuEntity : IEnuEntity
    {
        public virtual bool IsActive { get; set; }
    }

    public interface IEnuAuditEntity
    {
        DateTime CreatedOn { get; set; }
        string CreatedBy { get; set; }
        DateTime UpdatedOn { get; set; }
        string UpdatedBy { get; set; }
        bool IsDeleted { get; set; }
        DateTime? DeletedOn { get; set; }
        string DeletedBy { get; set; }

    }

    public abstract class EnuAuditEntity : BaseEnuEntity, IEnuAuditEntity
    {
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
    }
}
