﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENU.Entity.Common
{
    public class Facility : BaseEnuEntity
    {
        public int FacilityTypeId { get; set; }
        public string Name { get; set; }
    }
}
