﻿using System;
using Enu.Cms.BL;
using System.Web;
using ENU.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data;


namespace ENU.CMS.Controllers
{
    public class ScController : Controller
    {
        IDataAccessor accessor;
        Member mem = new Member();

        [HttpGet]
        public ActionResult All()
        {
            try
            {
                mem.AuthenticateUser();


                using (accessor = new DataAccessor())
                {
                    IInstitution inst = accessor.GetInstitution();
                    Institute institute;
                    institute = new Institute();
                    institute.InstituteType = "Tution Centre";
                    DataTable dataTable = inst.GetAllInstitutes(institute);
                    var list = new List<Tution_Centre>(from a in dataTable.AsEnumerable()
                                                       select new Tution_Centre
                                                   {
                                                       Name = Convert.ToString(a["Name"]),
                                                       Phone1 = Convert.ToString(a["Phone1"]),
                                                       Phone2 = Convert.ToString(a["Phone2"]),
                                                       Email = Convert.ToString(a["Email"]),
                                                       Address = Convert.ToString(a["Address"]),
                                                       EstablishedOn = Convert.ToInt32(a["EstablishmentYear"])
                                                   });
                    return View(list);
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                {
                    ViewBag.Message = "ERR";
                }


                return View(new List<Institute>());
            }
        }


        [HttpGet]
        public ActionResult Add()
        {
            try
            {
                mem.AuthenticateUser();
                return View();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                {
                    ViewBag.Message = "ERR";
                    return View();
                }
            }
        }


        [HttpPost]
        public ActionResult Add(FormCollection frmObj)
        {
            try
            {
                mem.AuthenticateUser();

                using (accessor = new DataAccessor())
                {
                    IInstitution inst = accessor.GetInstitution();
                    Institute institute = inst.TutionCentre();
                    institute.Name = frmObj["institute_name"];
                    institute.Address = frmObj["institute_adr"];
                    institute.State = int.Parse(frmObj["institute_state"]);
                    institute.City = int.Parse(frmObj["institute_city"]);
                    institute.Country = int.Parse(frmObj["institute_country"]);
                    institute.Description = frmObj["institute_desc"];
                    institute.Phone1 = frmObj["institute_phone1"];
                    institute.Phone2 = frmObj["institute_phone2"];
                    institute.Email = frmObj["institute_email"];
                    institute.WebUrl = frmObj["institute_webUrl"];
                    institute.EstablishedOn = int.Parse(frmObj["institute_esht"]);
                    //institute.AffiliationId = int.Parse(frmObj["institute_affliation"]);
                    institute.PinCode = frmObj["institute_pincode"];
                    institute.Keywords = "";
                    institute.ImgUrl = "";
                    inst.Save(institute);
                }

                ViewBag.Message = "OK";
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }


        [HttpGet]
        public ActionResult Edit()
        {
            try
            {
                mem.AuthenticateUser();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }
    }
}
