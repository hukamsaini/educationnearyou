﻿using System.Collections.Generic;
using System.Web.Mvc;
using Enu.Cms.BL;
using ENU.Entity.Common;
using ENU.CMS.ViewModel;
using System;

namespace ENU.CMS.Controllers
{
    public class MastersController : Controller
    {
        IDataAccessor accessor;

        [HttpGet]
        public ActionResult List()
        {
            return View(GetMasterDataViewModel());
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View(GetMasterDataViewModel());
        }

        [HttpPost]
        public ActionResult Add(MasterDataViewModel model)
        {

            try
            {
                using (accessor = new DataAccessor())
                {
                    IMasterDataHelper masterDataHelper = accessor.GetMasterDataHelper();
                    masterDataHelper.SaveMasterData(model.TypeId, model.TypeValue, model.StateId, model.CountryId);
                    TempData["Message"] = "Information Saved Successfully";
                    return RedirectToAction("add", "Masters");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message;
                return View(GetMasterDataViewModel());
            }
        }

        private MasterDataViewModel GetMasterDataViewModel()
        {
            var model = new MasterDataViewModel();
            using (accessor = new DataAccessor())
            {
                IMasterDataHelper masterDataHelper = accessor.GetMasterDataHelper();
                model.EnuMasterTypes = masterDataHelper.GetEnuMasterTypes();
                model.StateList = masterDataHelper.GetAllState();
                model.CountryList = masterDataHelper.GetAllCountry();
            }
            return model;

        }


        public PartialViewResult GetList(int typeId)
        {
            var model = GetMasterDataViewModel();
            string viewName = string.Empty;
            MasterTypes masterType = (MasterTypes)typeId;

            using (accessor = new DataAccessor())
            {
                IMasterDataHelper masterDataHelper = accessor.GetMasterDataHelper();

                switch (masterType)
                {
                    case MasterTypes.Subject:
                        viewName = "~/Views/Masters/SubjectList.cshtml";
                        model.SubjectList = masterDataHelper.GetAllSubject();
                        break;
                    case MasterTypes.CourseCategory:
                        viewName = "~/Views/Masters/CourseCategoryList.cshtml";
                        model.CourseCategoryList = masterDataHelper.GetAllCourseCategory();
                        break;
                    case MasterTypes.Facility:
                        viewName = "~/Views/Masters/FacilityList.cshtml";
                        model.FacilityList = masterDataHelper.GetAllFacility();
                        break;
                    case MasterTypes.City:
                        viewName = "~/Views/Masters/CityList.cshtml";
                        model.CityList = masterDataHelper.GetAllCity();
                        break;
                    case MasterTypes.State:
                        viewName = "~/Views/Masters/StateList.cshtml";
                        model.StateList = masterDataHelper.GetAllState();
                        break;
                    case MasterTypes.Country:
                        viewName = "~/Views/Masters/CountryList.cshtml";
                        model.CountryList = masterDataHelper.GetAllCountry();
                        break;
                    default:
                        viewName = "~/Views/Masters/FacilityList.cshtml";
                        break;
                }

            }
            return PartialView(viewName, model);
        }

        [HttpGet]
        public ActionResult Edit(int id, int typeId)
        {
            return View();
        }
    }
}
