﻿using System;
using System.Web;
using Enu.Cms.BL;
using ENU.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;


namespace ENU.CMS.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.Default)]
    public class HomeController : Controller
    {
        IDataAccessor accessor;

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Index(FormCollection frmObj)
        {
            try
            {
                if (frmObj == null)
                    return View();

                accessor = new DataAccessor();
                IUserAuthentication ua = accessor.GetUserAuthentication();
                UserTicket ticket = ua.LoginToCMS(frmObj["userName"], frmObj["userPassword"], string.Empty);

                if (ticket.Tocken.Contains(HttpContext.Session.SessionID))
                {
                    Session["ticket"] = ticket;
                    return this.RedirectToAction("dashboard", "mem");
                }
                else
                {
                    ViewBag.Message = "Invalid user name or password.";
                    return View();
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("USER_NOT_EXISTS"))
                    ViewBag.Message = "Invalid user name or password.";
                else
                    ViewBag.Message = "Failed to login to system. Please try again!";

                return View();
            }
        }


        [HttpGet]
        public ActionResult RetrievePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RetrievePassword(FormCollection frmObj)
        {
            return View();
        }
    }
}
