﻿using System;
using System.Web;
using Enu.Cms.BL;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace ENU.CMS.Controllers
{
    public class TutorialController : Controller
    {
        IDataAccessor accessor;
        Member mem = new Member();

        [HttpGet]
        public ActionResult Add()
        {
            try
            {
                mem.AuthenticateUser();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }


        [HttpPost]
        public ActionResult Add(FormCollection frmObj, HttpPostedFileBase tutorial_file)
        {
            try
            {
                mem.AuthenticateUser();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }

        [HttpGet]
        public ActionResult All()
        {
            try
            {
                mem.AuthenticateUser();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }
    }
}
