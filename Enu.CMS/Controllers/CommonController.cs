﻿using System;
using Enu.Cms.BL;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using ENU.CMS.ViewModel;
using ENU.Entity.Common;
using System.Collections.Generic;


namespace ENU.CMS.Controllers
{
    public class CommonController : Controller
    {
        IDataAccessor accessor;

        #region COMMON_METHODS

        /// <summary>
        /// Loads country list.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadCountry()
        {
            try
            {
                using (accessor = new DataAccessor())
                {
                    var model = new InstitutionViewModel();
                    IMasterDataHelper masterDataHelper = accessor.GetMasterDataHelper();
                    IInstitution inst = accessor.GetInstitution();
                    model.Countries = (List<Country>)masterDataHelper.GetAllCountry();
                    return Json(new { Status = "OK", Data = model.Countries });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = "ERR" });
            }
        }


        /// <summary>
        /// Loads state list of a country.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadState(int country)
        {
            try
            {
                using (accessor = new DataAccessor())
                {
                    var model = new InstitutionViewModel();
                    IMasterDataHelper masterDataHelper = accessor.GetMasterDataHelper();
                    IInstitution inst = accessor.GetInstitution();
                    model.States = (List<State>)masterDataHelper.GetAllState(country);
                    return Json(new { Status = "OK", Data = model.States });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = "ERR" });
            }
        }


        /// <summary>
        /// Loads state list of a country.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadCity(int state)
        {
            try
            {
                using (accessor = new DataAccessor())
                {
                    var model = new InstitutionViewModel();
                    IMasterDataHelper masterDataHelper = accessor.GetMasterDataHelper();
                    IInstitution inst = accessor.GetInstitution();
                    model.Cities = (List<City>)masterDataHelper.GetAllCity(state);
                    return Json(new { Status = "OK", Data = model.Cities });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = "ERR" });
            }
        }


        /// <summary>
        /// Loads institution types.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadInstitution()
        {
            try
            {
                using (accessor = new DataAccessor())
                {
                    var model = new InstitutionViewModel();
                    IMasterDataHelper masterDataHelper = accessor.GetMasterDataHelper();
                    IInstitution inst = accessor.GetInstitution();
                    model.EnuCategories = (List<EnuCategories>)masterDataHelper.GetEnuCategories();
                    return Json(new { Status = "OK", Data = model.EnuCategories });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = "ERR" });
            }
        }


        /// <summary>
        /// Get list of board/unoversities in name-value pair only.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult LoadBoardList(string type)
        {
            try
            {
                using (accessor = new DataAccessor())
                {
                    ICommonMaster master = accessor.GetCommonMaster();
                    List<Board> boards = master.GetAllBoard(Char.Parse(type.Substring(0, 1)), null, string.Empty);
                    return Json(new { Status = "OK", Data = boards });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = "ERR" });
            }
        }

        #endregion COMMON_METHODS
    }
}