﻿using System;
using Enu.Cms.BL;
using System.Web;
using ENU.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;


namespace ENU.CMS.Controllers
{
    public class BoardController : Controller
    {
        IDataAccessor accessor;
        Member mem = new Member();


        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Add(FormCollection frmObj)
        {
            try
            {
                mem.AuthenticateUser();

                //if (frmObj["cancel"].ToUpper() == "CANCEL")
                //    return this.RedirectToAction("boards", "board");

                if (string.IsNullOrEmpty(frmObj["board_name"]))
                {
                    ViewBag.Message = "Please enter board/university name and then try.";
                    return View();
                }

                using (accessor = new DataAccessor())
                {
                    ICommonMaster mstr = accessor.GetCommonMaster();
                    Board board = mstr.GetBoard();
                    board.Name = frmObj["board_name"];
                    board.Value = frmObj["board_val"];
                    board.Type = Char.Parse(frmObj["board"].ToUpper());
                    mstr.Save(board);
                }

                ViewBag.Message = "OK";
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }


        [HttpGet]
        public ActionResult All()
        {
            try
            {
                mem.AuthenticateUser();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }


        [HttpPost]
        public JsonResult LoadAllBoards(string type, int? value)
        {
            try
            {
                if (string.IsNullOrEmpty(type))
                    return Json(null);

                using (accessor = new DataAccessor())
                {
                    ICommonMaster master = accessor.GetCommonMaster();
                    List<Board> boards = master.GetAllBoard(Char.Parse(type.Substring(0, 1)), value, Enu.Cms.BL.Action.GET.ToString());
                    return Json(boards);
                }
            }
            catch
            {
                return Json(null);
            }
        }
    }
}
