﻿using System;
using ENU.Entity;
using Enu.Cms.BL;
using System.Web;
using System.Web.Mvc;
using ENU.CMS.ViewModel;
using ENU.Entity.Common;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Data;

namespace ENU.CMS.Controllers
{
    public class MemController : Controller
    {
        IDataAccessor accessor;
        Member mem = new Member();

        [HttpGet]
        public ActionResult Dashboard()
        {
            try
            {
                mem.AuthenticateUser();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }

        [HttpGet]
        public ActionResult Institutions()
        {
            //InstitutionType insType = new InstitutionType();

            try
            {
                mem.AuthenticateUser();


                using (accessor = new DataAccessor())
                {
                    IInstitution inst = accessor.GetInstitution();
                    Institute institute;
                    institute = new Institute();
                    institute.InstituteType = "school";
                    DataTable dataTable = inst.GetAllInstitutes(institute);
                    var list = new List<Institute>(from a in dataTable.AsEnumerable()
                                                   select new Institute { Name = a["Name"].ToString(), Description = a["Desc"].ToString() });
                    return View(list);
                }               

            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                {
                    ViewBag.Message = "ERR";
                }


                return View(new List<Institute>());
            }
        }

        [HttpGet]
        public ActionResult AddInstitution()
        {
            try
            {
                mem.AuthenticateUser();
                return View();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                {
                    ViewBag.Message = "ERR";
                    return View();
                }
            }
        }


        [HttpGet]
        public ActionResult Logout()
        {
            if (Session["ticket"] != null)
            {
                accessor = new DataAccessor();
                IUserAuthentication ua = accessor.GetUserAuthentication();
                ua.Logout(string.Empty);
            }

            return mem.LogoutUser();
        }


        public PartialViewResult GetInstituitonView(int categoryId)
        {
            var model = new InstitutionViewModel();
            string viewName = string.Empty;
            switch (categoryId)
            {
                case 1:
                    viewName = "~/Views/Mem/components/School.cshtml";
                    break;
                case 2:
                    viewName = "~/Views/Mem/components/College.cshtml";
                    break;
                case 3:
                    viewName = "~/Views/Mem/components/TutionCentre.cshtml";
                    break;
                default:
                    viewName = "~/Views/Mem/components/School.cshtml";
                    break;
            }
            return PartialView(viewName, GetInstitutionViewModel());
        }


        [HttpPost]
        public ActionResult AddInstitution(FormCollection frmObj)
        {
            try
            {
                mem.AuthenticateUser();

                using (accessor = new DataAccessor())
                {
                    IInstitution inst = accessor.GetInstitution();
                    Institute institute;

                    if (frmObj["institute_type"].ToLower() == "school")
                        institute = inst.School();
                    else if (frmObj["institute_type"].ToLower() == "college")
                        institute = inst.College();
                    else if (frmObj["institute_type"].ToLower() == "tution centre")
                        institute = new Tution_Centre();
                    else
                        institute = new Institute();

                    institute.Name = frmObj["institute_name"];
                    institute.Address = frmObj["institute_adr"];
                    institute.State = int.Parse(frmObj["institute_state"]);
                    institute.City = int.Parse(frmObj["institute_city"]);
                    institute.Country = int.Parse(frmObj["institute_country"]);
                    institute.Description = frmObj["institute_desc"];
                    institute.Phone1 = frmObj["institute_phone1"];
                    institute.Phone2 = frmObj["institute_phone2"];
                    institute.Email = frmObj["institute_email"];
                    institute.WebUrl = frmObj["institute_webUrl"];
                    institute.EstablishedOn = int.Parse(frmObj["institute_esht"]);
                    institute.AffiliationId = int.Parse(frmObj["institute_affliation"]);
                    institute.PinCode = "";
                    institute.Keywords = "";
                    institute.ImgUrl = "";
                    inst.Save(institute);
                }

                ViewBag.Message = "OK";
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View(GetInstitutionViewModel());
        }


        //TODO Later on it will have another filter like Active and all 
        [HttpGet]
        public JsonResult GetInstitution(string instituteType)
        {
            try
            {
                mem.AuthenticateUser();

                using (accessor = new DataAccessor())
                {
                    IInstitution inst = accessor.GetInstitution();
                    Institute institute;
                    //String instituteType = "school";
                    institute = new Institute();
                    //TODO Need to discuss view model implemetaation with Pandit 
                    //Pandit ceck this
                    institute.InstituteType = instituteType;
                    var dataTable = inst.GetAllInstitutes(institute);
                    //return View(dataTable);
                    //  return  JsonConvert.SerializeObject(ModelMapper.ConvertDataTable<InsititueViewModel>(dataTable), Formatting.Indented);
                    return Json(ModelMapper.ConvertDataTable<InsititueViewModel>(dataTable), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    Session.Clear();
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ViewBag.Message = "ERR";
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
        }


        [HttpGet]
        public ActionResult AddUser()
        {
            try
            {
                mem.AuthenticateUser();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }


        [HttpPost]
        public ActionResult AddUser(FormCollection fromObj)
        {
            try
            {
                mem.AuthenticateUser();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }

        [HttpGet]
        public ActionResult AllUsers()
        {
            try
            {
                mem.AuthenticateUser();
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("UNKOWN_USER"))
                {
                    return mem.LogoutUser();
                }
                else
                    ViewBag.Message = "ERR";
            }

            return View();
        }


        private InstitutionViewModel GetInstitutionViewModel()
        {
            var model = new InstitutionViewModel();
            using (accessor = new DataAccessor())
            {
                IMasterDataHelper masterDataHelper = accessor.GetMasterDataHelper();
                IInstitution inst = accessor.GetInstitution();
                model.EnuCategories = (List<EnuCategories>)masterDataHelper.GetEnuCategories();

                model.Cities = (List<City>)masterDataHelper.GetAllCity(0);
                model.States = (List<State>)masterDataHelper.GetAllState(81);
                model.Countries = (List<Country>)masterDataHelper.GetAllCountry();
            }

            return model;
        }


        [HttpGet]
        public ActionResult Tutorial()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Tutorial(FormCollection frmObj, HttpPostedFileBase tutorial_file)
        {
            return View();
        }
    }
}
