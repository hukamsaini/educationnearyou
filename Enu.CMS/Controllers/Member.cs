﻿using System;
using System.Web;
using System.Web.Mvc;


namespace ENU.CMS.Controllers
{
    public class Member : Controller
    {
        /// <summary>
        /// Authenticate user.
        /// </summary>
        public void AuthenticateUser()
        {
            if (System.Web.HttpContext.Current.Session["ticket"] == null)
            {
                throw new ArgumentException("UNKOWN_USER");
            }
        }

        /// <summary>
        /// Logout user.
        /// </summary>
        /// <returns></returns>
        public ActionResult LogoutUser()
        {
            System.Web.HttpContext.Current.Session.Clear();
            System.Web.HttpContext.Current.Session.Abandon();
            return this.RedirectToAction("index", "home");
        }
    }
}