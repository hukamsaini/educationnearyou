﻿var enuManager =
{
    validateForm: function (form) {
        var isValid = this.processForm(form);
        if (isValid) {
            $(form).submit();
            return true;
        }
        return false;
    },

    processForm: function (form) {
        var value;
        var divError = $(form).find("#errorMsg");
        $(divError).html('');

        var errorMessage = "";
        var validate = true;

        $(form).find(".required").not('span').each(function (index, elm) {
            var result;
            switch (elm.type) {
                case 'select-one':
                    value = elm.value;
                    result = (value == 0);
                    break;

                case 'checkbox':
                    value = $(elm).prop('checked');
                    result = (value == null || !value);
                    break;

                default:
                    value = elm.value;
                    value = value ? $.trim(value) : value;
                    result = (value == null || value == '');
                    break;
            }

            if (result) {
                $(elm).addClass('error');
                $(divError).addClass('alert alert-danger');

                if ($(this).attr('data-error-message') != null) {
                    errorMessage = errorMessage + "<li><span>" + $(this).attr('data-error-message') + "</span></li>";
                }
                else {
                    errorMessage = errorMessage + "<li><span> '" + $(this).attr('data-friendly-name') + "' is required.</span></li>";
                }
                validate = false;
            }
            else {
                $(elm).removeClass('error');
                $(divError).removeClass('alert alert-danger');
            }

        });

        $(form).find('.email').not('span').each(function (index, elm) {
            var result;
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (elm.value) {
                if (!reg.test(elm.value)) {
                    result = true;
                }

                if (result) {
                    $(elm).addClass('error');
                    $(divError).addClass('alert alert-danger');
                    if ($(this).attr('data-error-message') != null) {
                        errorMessage = errorMessage + "<li><span>" + $(this).attr('data-error-message') + "</span></li>";
                    }
                    else {
                        errorMessage = errorMessage + "<li><span> Please enter valid '" + $(this).attr('data-friendly-name') + "' </span></li>";
                    }
                    validate = false;
                } else {
                    $(elm).removeClass('error');
                    $(divError).removeClass('alert alert-danger');
                }
            }
        });

        $(form).find('.phone').not('span').each(function (index, elm) {

            if (elm.value != null && elm.value != '') {
                var result;

                var reg = /^[\d\s]+$/;
                if (!reg.test(elm.value)) {
                    result = true;
                }
                if (result) {
                    $(elm).addClass('error');
                    $(divError).addClass('alert alert-danger');

                    if ($(this).attr('data-error-message') != null) {
                        errorMessage = errorMessage + "<li><span>" + $(this).attr('data-error-message') + "</span></li>";
                    }
                    else {
                        errorMessage = errorMessage + "<li><span> Please enter valid '" + $(this).attr('data-friendly-name') + "' </span></li>";
                    }
                    validate = false;
                } else {
                    $(elm).removeClass('error');
                    $(divError).removeClass('alert alert-danger');
                }
            }
        });

        if (!validate) {
            $(divError).addClass('alert alert-danger');
            $(divError).html(errorMessage);
            $(divError).show();
            return false;
        }
        return true;
    }
};