﻿var schoolManager = {

    resetAll: function () {
        alert('reset');
    },

    save: function (form) {

        var result = enuManager.validateForm(form);
        if (!result)
            return;
        var model = {};
        model.Name = $("#schoolName").val();
        model.Desc = $("#Description").val();
        model.Address = $("#address").val();
        model.City = $("#city").val();
        model.State = $("#state").val();
        model.Phone1 = $("#Phone1").val();
        model.Phone2 = $("#Phone2").val();
        model.Email = $("#email").val();
        model.Website = $("#Website").val();
        model.EstablishmentYear = $("#establishmentYear").val();
        model.Affliation = $("#Affliation").val();
        model.Keywords = $("#keywords").val();      

        $.ajax({
            url: "/School/NewSchool",
            type: "POST",
            data: JSON.stringify(model),
            dataType: "json",
            contentType: 'application/json',
            //timeout: options.timeout,
            //async: options.async,
            success: function (result) {
            },
            error: function () { }
        });
    }
};