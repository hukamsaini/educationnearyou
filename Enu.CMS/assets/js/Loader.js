﻿/* Load list of institution types */
function LoadBoardList(institute) {
    $.ajax({
        url: '../common/LoadBoardList',
        type: 'POST',
        data: { type: institute },

        datatype: "json",
        contenttype: "application/json; charset=utf-8",

        processdata: false,
        success: function (result) {
            var html = '<option selected="selected" value="">Select</option>';
            $.each(result.Data, function (b) {
                html = html + '<option value="' + result.Data[b].Value + '">' + result.Data[b].Name + '</option>';
            });

            $("#institute_affliation").html(html);
        },
        error: function () {
            $("#institute_affliation").html('<option selected="selected" value="">Select Country</option>');
        }
    });
}

/* Load list of institution types */
//function LoadInstitution() {
//    $.ajax({
//        url: '../common/LoadInstitution',
//        type: 'POST',
//        data: {},

//        datatype: "json",
//        contenttype: "application/json; charset=utf-8",

//        processdata: false,
//        success: function (result) {
//            var html = '<option selected="selected" value="">Select Institution Type</option>';
//            $.each(result.Data, function (b) {
//                html = html + '<option value="' + result.Data[b].CategoryName + '">' + result.Data[b].CategoryName + '</option>';
//            });

//            $("#institute_type").html(html);
//        },
//        error: function () {
//            $("#institute_type").html('<option selected="selected" value="">Select Country</option>');
//        }
//    });
//}

/* Load list of boards and universities */
function LoadAllBoards() {
    $.ajax({
        url: '../board/LoadAllBoards',
        type: 'POST',
        data: { type: $("[name=institute_type]").val(), value: null },

        datatype: "json",
        contenttype: "application/json; charset=utf-8",

        processdata: false,
        success: function (result) {
            var html = '';
            $.each(result, function (b) {
                html = html + '<tr><td class="check-column" style="width: 70%">' + result[b].Name + '</td><td style="width: 15%">' + result[b].Active + '</td><td class="action-column" style="width: 15%"><a href="/mem/boards?for=' + result[b].Value + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a href="#"><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr>';
            });

            $("#lst_boards").html(html);
        },
        error: function () {
            $("#lst_boards").html('<tr><td class="check-column" style="width: 70%">Error while loading data.</td></tr>');
        }
    });
}

/* Loads list of countries */
function LoadCountry() {
    $.ajax({
        url: '../common/LoadCountry',
        type: 'POST',
        data: {},

        datatype: "json",
        contenttype: "application/json; charset=utf-8",

        processdata: false,
        success: function (result) {
            var html = '<option selected="selected" value="">Select Country</option>';
            $.each(result.Data, function (b) {
                html = html + '<option value="' + result.Data[b].CountryId + '">' + result.Data[b].CountryName + '</option>';
            });

            $("#lst_country").html(html);
        },
        error: function () {
            $("#lst_country").html('<option selected="selected" value="">Select Country</option>');
        }
    });
}


/* Loads list of state in a country */
function LoadState() {
    $.ajax({
        url: '../common/LoadState',
        type: 'POST',
        data: { country: $("#lst_country").val() },

        datatype: "json",
        contenttype: "application/json; charset=utf-8",

        processdata: false,
        success: function (result) {
            var html = '<option selected="selected" value="">Select State</option>';
            $.each(result.Data, function (b) {
                html = html + '<option value="' + result.Data[b].StateId + '">' + result.Data[b].StateName + '</option>';
            });

            $("#lst_state").html(html);
        },
        error: function () {
            $("#lst_state").html('<option selected="selected" value="">Select Country</option>');
        }
    });
}


/* Loads list of city in a state */
function LoadCity() {
    $.ajax({
        url: '../common/LoadCity',
        type: 'POST',
        data: { state: $("#lst_state").val() },

        datatype: "json",
        contenttype: "application/json; charset=utf-8",

        processdata: false,
        success: function (result) {
            var html = '<option selected="selected" value="">Select State</option>';
            $.each(result.Data, function (b) {
                html = html + '<option value="' + result.Data[b].CityId + '">' + result.Data[b].CityName + '</option>';
            });

            $("#lst_city").html(html);
        },
        error: function () {
            $("#lst_city").html('<option selected="selected" value="">Select Country</option>');
        }
    });
}

$("#institute_type").change(function () {
    $("#load_institution").load('/' + $("#institute_type").val().replace(' ', '') + '.htm');
    $("#dic_post").show();
    $(".alert").html('');
    $(".alert").hide();
    LoadCountry();
});


$("#lst_country").change(function () {
    LoadState();
});

$("#lst_state").change(function () {
    LoadCity();
});