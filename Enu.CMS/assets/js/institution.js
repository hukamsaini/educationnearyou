﻿$(document).ready(function () {
    $("[name=institute_type]").change(function () {
        $("#load_institution").load('/' + $("[name=institute_type]").val().replace(' ', '') + '.htm');
        $("#dic_post").show();
        $(".alert").html('');
        $(".alert").hide();
    });
});    