﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ENU.Entity;
using ENU.Entity.Common;
namespace ENU.CMS.ViewModel
{
    public class InstitutionViewModel
    {
        public List<EnuCategories> EnuCategories { get; set; }
        public List<State> States { get; set; }
        public List<City> Cities { get; set; }
        public List<Country> Countries { get; set; }
    }
}