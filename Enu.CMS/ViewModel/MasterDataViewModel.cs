﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ENU.Entity.Common;

namespace ENU.CMS.ViewModel
{
    public class MasterDataViewModel
    {
        public List<EnuMasterType> EnuMasterTypes { get; set; }
        public int TypeId { get; set; }
        public string TypeValue { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }

        public List<Subject> SubjectList { get; set; }
        public List<Facility> FacilityList { get; set; }
        public List<CourseCategory> CourseCategoryList { get; set; }
        public List<City> CityList { get; set; }
        public List<State> StateList { get; set; }
        public List<Country> CountryList { get; set; }
    }
}