﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Reflection;

namespace ENU.CMS.ViewModel
{
    public static class ModelMapper
    {
        /// <summary>
        /// This method will convert datatable to a list.
        /// </summary>
        /// <param name="command">Datatble</param>
        /// <returns>List of object</returns>
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        internal static object ConvertDataTable<T1>(IList<Entity.Institute> dataTable)
        {
            throw new NotImplementedException();
        }
    }
}
