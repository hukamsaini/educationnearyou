﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ENU.Entity;

namespace ENU.CMS.ViewModel
{
    public class InsititueViewModel
   {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Affiliation { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string WebUrl { get; set; }
        public string ImgUrl { get; set; }
        public string Address { get; set; }
        public int State { get; set; }
        public int City { get; set; }
        public int Country { get; set; }
        public int EstablishedOn { get; set; }
        public string InstituteType { get; set; }
        public Nullable<int> InstituteId { get; set; }
        public string Keywords { get; set; }
        public string CountryName { get; set; }
        public string StateName { get;set;}
        public string CityName { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
    }
 }
